// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

    
template <typename T>
struct DequeDequeFixture : Test {
    using deque_deque_type    = T;
    using allocator_type = typename deque_deque_type::allocator_type;
    using iterator       = typename deque_deque_type::iterator;
    using const_iterator = typename deque_deque_type::const_iterator;};

using deque_types =
    Types<
        deque<int>,
        my_deque<int>,
        deque<int, allocator<int>>,
        my_deque<int, allocator<int>>
        //   deque<deque<int>>,
        //my_deque<deque<int>>,
        //   deque<deque<int>, allocator<deque<int>>>
        //my_deque<deque<int>, allocator<deque<int>>>
        >;

using deque_deque_types =
    Types<deque<deque<int>>,
        my_deque<deque<int>>,
        deque<deque<int>, allocator<deque<int>>>,
        my_deque<deque<int>, allocator<deque<int>>>
        >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types);
    TYPED_TEST_CASE(DequeDequeFixture, deque_deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
    TYPED_TEST_CASE(DequeDequeFixture, deque_deque_types);
#endif

// #ifdef __APPLE__
    
// #else
//     TYPED_TEST_CASE(DequeDequeFixture, deque_deque_types);
// #endif

/**
 * 
 * Things to test (Either directly or indirectly, including const counterpart):
 * ==  X
 * <   X
 * swap X
 * rel_ops X
 * + X
 * - X
 * * X
 * -> idk
 * ++ pre X
 * ++ post X
 * -- pre X
 * -- post X
 * += X
 * -= X
 * all flavors of constructor X
 * destructor X
 * assignment X
 * indexing [] X
 * at X
 * back X
 * clear X
 * erase X
 * front X
 * insert X
 * pop_back X
 * pop_front X
 * push_back X
 * push_front X
 * resize X
 * 
 **/

// -----
// Tests
// -----

// template for normal test

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

// template for iterator
TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
    }

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

/**
 * Tests begin, end, size, empty and ++ and -- for iterators
 **/
TYPED_TEST(DequeFixture, test5) {
    //tests iterator operations of pre increment and decrement
    //also tests the size constructor
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2);

    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_FALSE(x.empty());
    ASSERT_EQ(x.size(), 2);
    ++b;
    --e;
    EXPECT_TRUE(b == e);
}

/*
* tests * (write), += and -=  and post increment/decrement
* also tests the initializer list constructor
*/
TYPED_TEST(DequeFixture, test6) {

    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    
    const initializer_list<int> x = {1, 2, 3, 4, 5};
    ASSERT_EQ(x.size(), 5u);
    EXPECT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
    
    deque_type y(x);
    iterator b = begin(y);
    iterator e = end(y);
    ASSERT_FALSE(y.empty());
    ASSERT_EQ(y.size(), 5u);

    b++;

    e--;

    *b = 6;

    EXPECT_TRUE(equal(b, e, begin({6, 3, 4})));


    b += 2;
    e -= 1;

    EXPECT_TRUE(b == e);
}

/*
* tests * (read), += and -=  and post increment/decrement for const iterator
* also tests the initializer list constructor
*/

TYPED_TEST(DequeFixture, test7) {

    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename deque_type::const_iterator;
    
    const initializer_list<int> x = {1, 2, 3, 4, 5};
    ASSERT_EQ(x.size(), 5u);
    EXPECT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
    
    const deque_type y(x);
    const_iterator b = begin(y);
    const_iterator e = end(y);

    ASSERT_FALSE(y.empty());
    ASSERT_EQ(y.size(), 5u);

    b++;
    e--;

    EXPECT_TRUE(*b == 2);

    b += 2;
    e -= 1;

    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    
    const initializer_list<int> x = {1, 2, 3, 4, 5};   
    deque_type y = x;

    ASSERT_FALSE(y.empty());
    ASSERT_EQ(y.size(), 5u);

    y.clear();
    ASSERT_EQ(y.size(), 0u);
    ASSERT_TRUE(y.empty());
}

/*
* Tests front, back, and push front
* Checks that front value changes if deque has something added to the front
* Checking the constant front method
*/ 

TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    
    const initializer_list<int> x = {1, 2, 3, 4, 5};   
    deque_type y = x;

    ASSERT_FALSE(y.empty());
    ASSERT_EQ(y.size(), 5u);

    ASSERT_EQ(y.front(), 1u);
    ASSERT_EQ(y.back(), 5u);
    y.push_front(6);
    ASSERT_EQ(y.front(), 6u);
    ASSERT_EQ(y.size(), 6u);
    
}

/*
* Tests front, back non const methods
* Also checks that using these methods updates values in deque
*/
TYPED_TEST(DequeFixture, test10) {
    
    using deque_type = typename TestFixture::deque_type;
    
    const initializer_list<int> x = {1, 2, 3, 4, 5};   
    deque_type y = x;

    ASSERT_FALSE(y.empty());
    ASSERT_EQ(y.size(), 5u);
 
    y.front() = 6u;
    ASSERT_EQ(y[0u], 6u);
    
    y.back() = 9u;
    ASSERT_EQ(y[4], 9u);

    ASSERT_EQ(y.size(), 5u);
    
}

/*
    Tests push front on an empty deque
    Checks that front and back values are updated properly
*/

TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_front(i);
        ASSERT_EQ(x.front(), i);
    }
    ASSERT_EQ(x.back(), 0);
}

/*
Ensures that a deque of size one has the same front and back values
*/
TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x (1, 1);
    ASSERT_EQ(x.front(), x.back());}


//Tests pop_front to make sure deque is properly removing objects and updating size
TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 1);
    for(int i = 0; i < 10; ++i) {
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 0);
    }

//Tests erase, back, front and size. 
//Checks to see if erase updates back and front if erasing at b or e 
TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;

    const initializer_list<int> x = {1, 2, 3, 4, 5};   
    deque_type y = x;
    iterator b = begin(y);
    y.erase(b);
    iterator e = end(y);
    y.erase(--e);
    ASSERT_EQ(y.size(), 3u);
    ASSERT_EQ(y.front(), 2);
    ASSERT_EQ(y.back(), 4);
}

/*
*    Tests that resizing into a larger deque calls default constructor 
*    for values and updates size accordingly
*/
TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 1);
    x.resize(20);
    ASSERT_EQ(x.size(), 20);   
}

/**
 * Tests that pushing and popping works in either direction
 **/
TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    x.pop_front();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

/**
 * Tests --, ++, for iterator; begin and end for deque
 **/
TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10, 1);
    iterator b = begin(x);
    iterator e = end(x);
    --e;
    while (b != e && b != e - 1) {
        int sum = *e + *b;
        ++b;
        --e;
        *e = sum;
        *b = sum;
    }
    ASSERT_EQ(*b, 16);
}

/**
 * Tests size
 **/
TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1);
    ASSERT_TRUE(x.size() == 1);
}

/**
 * Tests indexing, begin, end and iterator -- and ++
 **/
TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(100, 1);
    iterator b = begin(x);
    iterator e = end(x);
    for (int i = 0; i < 100; ++i) {
        if (i % 2 == 1) {
            x[i] += i;
        }
    }
    while (b != e) {
        if (*b % 2 == 1) {
            --e;
        } else {
            ++b;
        }
    }
    ASSERT_EQ(*b % 2, 1);
}

/**
 * Tests clear
 **/
TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 100);
    x.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

/**
 * Tests iterator ++, -- functionalities
 **/
TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(109, 1);
    iterator b = begin(x);
    iterator e = end(x);
    --e;
    while (b != e) {
        ++*b;
        ++b;
        *e = *b + 1;
        --e;
    }
    ASSERT_TRUE(x.back() == 2);
}

/**
 * Next two tests test constructors that accept allocators
 **/
TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    const deque_type x(100, 10, allocator_type());
    ASSERT_EQ(x.size(), 100u);}

TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x({5, 6, 4, 2, 1}, allocator_type());
    ASSERT_EQ(x[4], 1);
}

/**
 * Tests begin, erase, and size as well as + for iterator
 **/
TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1000000);
    iterator pos = begin(x) + 500000;
    x.erase(pos);
    ASSERT_EQ(x.size(), 999999u);
}

/**
 * Tests indexing for const deque
 **/
TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type y(1);
    ASSERT_EQ(0, y[0]);
}

/**
 * Tests indexing and back
 **/
TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 10);
    for(size_t i = 0; i < x.size(); ++i) {
        x[i] += i;
    }
    ASSERT_EQ(x.back(), 109);
}

/**
 * Tests at for const deque
 **/
TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x({0, 9, 1, 3, 9});
    ASSERT_EQ(x.at(0), 0);
}

/**
 * Tests swapping
 **/
TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({4, 5, 6, 7, 8});
    deque_type y({9, 10, 11, 12, 13});
    deque_type z(x);
    y.swap(z);
    ASSERT_TRUE(x == y);
}

/**
 * The next four tests test the comparison operations >=, <=, >, and <.
 **/
TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({4, 5, 6});
    const deque_type y({5, 6, 7});
    ++x.front();
    ++x.at(1);
    ++x.at(2);
    ASSERT_GE(x, y);
}


TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({4, 5, 6});
    const deque_type y({5, 6, 7});
    ++x.front();
    ASSERT_LE(x, y);
}

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x({4, 5, 6});
    const deque_type y({5, 6, 7});
    ASSERT_GT(y, x);
}

TYPED_TEST(DequeFixture, test32) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x({4, 5, 6});
    const deque_type y({5, 6, 7});
    ASSERT_LT(x, y);
}

/**
 * Tests + for iterator
 **/
TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(7);
    iterator b = begin(x);
    iterator e = end(x);
    b += 7;
    EXPECT_TRUE(b == e);
}

/**
 * Tests - for iterator
 **/
TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename deque_type::const_iterator;
    const deque_type x(6);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    e -= 6;
    EXPECT_TRUE(b == e);
}

/**
 * Tests begin, end, += for iterators as well as at
 **/
TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(120, 1);
    iterator b = begin(x);
    iterator e = end(x);
    int index = 0;
    while (b != e) {
        *b += index;
        ++index;
        b += index;
    }
    EXPECT_TRUE(b == e);
    ASSERT_EQ(x.at(105), 15);
}

/**
 * Tests insert, back, and pop_back as well as validity of iterators
 **/
TYPED_TEST(DequeFixture, test36) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(9, 12);
    iterator b = begin(x) + (x.size() / 2);
    while (x.back() >= 12) {
        x.insert(b, --x.front());
        x.pop_back();
    }
    ASSERT_EQ(x.back(), 11);
}

/**
 * Tests iterator decrementation and at
 **/
TYPED_TEST(DequeFixture, test37) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(3, 3);
    iterator e = end(x);
    ----e;
    x.at(1) = 2;
    ASSERT_EQ(*e, 2);
    const deque_type y = {3, 4, 5};
    ASSERT_EQ(x.at(0), y.at(0));
    }

/**
 * Tests push_back affecting size
 **/
TYPED_TEST(DequeFixture, test38) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(100, 10);
    deque_type y(99, 10);
    y = x;
    y.push_back(y.size());
    ASSERT_EQ(y.size(), 101u);}

/**
 * Tests dereferencing and at
 **/
TYPED_TEST(DequeFixture, test39) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1000);
    x.at(100) = 100;
    deque_type y(1000);
    ASSERT_NE(x, y);
    iterator b = begin(x);
    while (*b != 100) {
        ++*b;
        ++b;
    }
    x.resize(100);
    y = deque_type(100, 1);
    ASSERT_EQ(x, y);
    ASSERT_EQ(y.size(), 100u);
}

/**
 * Tests that pop affects size.
 **/
TYPED_TEST(DequeFixture, test40) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(10, 20);
    deque_type y(10, 20);
    ASSERT_EQ(x, y);
    y.pop_back();
    y.pop_front();
    ASSERT_EQ(y.size(), 8u);
}
