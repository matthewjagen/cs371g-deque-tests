// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    // Types<
    //        deque<int>,
    //     my_deque<int>,
    //        deque<int, allocator<int>>,
    //     my_deque<int, allocator<int>>,
    //        deque<deque<int>>,
    //     my_deque<deque<int>>,
    //        deque<deque<int>, allocator<deque<int>>>,
    //     my_deque<deque<int>, allocator<deque<int>>>>;

    // Used for testing std::deque to ensure correctness of myDeque later on
    // Types<deque<int>>;
    Types<deque<int>, my_deque<int, allocator<int>>, my_deque<int>>; // my_deque<int>, deque<deque<int>>, deque<deque<int>, allocator<deque<int>>>>;

// Will use seperate tests for deques of deques

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// --------------------------------------------------------------------------------
// Tests 1-4: Provided Tests
// --------------------------------------------------------------------------------

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.begin(), x.end());
}

// --------------------------------------------------------------------------------
// Tests 5-6: Basic push_back(), push_front(), size(), iterator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.size(), 0);
    x.push_back(3);
    x.push_back(4);
    ASSERT_EQ(*x.begin(), 3);
    ASSERT_EQ(*--x.end(), 4);
    x.push_front(2);
    x.push_front(1);
    ASSERT_EQ(*x.begin(), 1);
    ASSERT_EQ(*--x.end(), 4);
    ASSERT_EQ(x.size(), 4);
}

// --------------------------------------------------------------------------------
// Tests 7-12: Larger push_back(), push_front(), size(), iterator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    for(int i = 1; i <= 100; i++) {
        x.push_back(i);
    }
    ASSERT_EQ(*x.begin(), 0);
    ASSERT_EQ(*--x.end(), 100);
    ASSERT_EQ(x.size(), 101);
    for(int i = 1; i <= 100; ++i) {
        x.push_front(i);
    }
    ASSERT_EQ(*x.begin(), 100);
    ASSERT_EQ(x.size(), 201);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 1000; ++i) {
        x.push_front(i);
    }
    ASSERT_EQ(x.front(), 999);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 1000; ++i) {
        x.push_back(i);
    }
    for(int i = 0; i < 1000; ++i) {
        ASSERT_EQ(x[i], i);
    }
}

// --------------------------------------------------------------------------------
// Tests 13-14: empty(), clear(), erase(), pop_front(), pop_back() tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test6) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.empty(), true);
    for(int i = 0; i < 20; ++i) {
        x.push_back(i);
    }
    ASSERT_EQ(x.empty(), false);
    x.erase(x.begin() + 5);
    ASSERT_EQ(*(x.begin() + 5), 6);
    for(int i = 0; i < 4; ++i) {
        x.pop_front();
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 11);
    x.clear();
    ASSERT_EQ(x.begin(), x.end());
    ASSERT_EQ(x.empty(), true);
}

// --------------------------------------------------------------------------------
// Tests 15-16: at(), [] tests on non-modified, small deque
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test7) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10);
    for(int i = 0; i < 10; ++i) {
        x[i] = i;
        ASSERT_EQ(x.at(i), i);
        ASSERT_EQ(x.size(), 10);
    }
    ASSERT_EQ(*x.begin(), 0);
    ASSERT_EQ(*--x.end(), 9);
}

// --------------------------------------------------------------------------------
// Tests 17-18: at(), [] tests on non-modified, large deque
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test8) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(100);
    for(int i = 0; i < 100; ++i) {
        x[i] = i;
        ASSERT_EQ(x.at(i), i);
        ASSERT_EQ(x.size(), 100);
    }
    ASSERT_EQ(*x.begin(), 0);
    ASSERT_EQ(*--x.end(), 99);
}

// --------------------------------------------------------------------------------
// Tests 19-20: at(), [] tests on modified, small deque
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test9) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_back(i);
    }
    ASSERT_EQ(x.at(4), 4);
    ASSERT_EQ(x[4], 4);
    for(int i = 0; i < 5; ++i) {
        x.push_front(-i);
    }
    ASSERT_EQ(x.at(0), -4);
    ASSERT_EQ(x[4], 0);
    ASSERT_EQ(x.at(13), 8);
    ASSERT_EQ(x.at(7), 2);
}

// --------------------------------------------------------------------------------
// Tests 21-22: at(), [] tests on modified, large deque
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test10) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    ASSERT_EQ(x.at(40), 40);
    ASSERT_EQ(x[40], 40);
    for(int i = 0; i < 50; ++i) {
        x.push_front(-i);
    }
    ASSERT_EQ(x.at(0), -49);
    ASSERT_EQ(x[50], 0);
    ASSERT_EQ(x.at(130), 80);
    ASSERT_EQ(x.at(70), 20);
}

// --------------------------------------------------------------------------------
// Tests 23-24: iterator operator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test11) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(10);
    iterator i = x.begin();
    x.push_back(1);
    ASSERT_EQ(x.size(), 11);
    ASSERT_EQ(*i, 0);
    ASSERT_EQ(*(i + 10), 1);
}

// --------------------------------------------------------------------------------
// Tests 25-26: more iterator operator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test12) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(200, 1);
    iterator i = x.begin();
    x.push_back(0);
    ASSERT_EQ(x.size(), 201);
    ASSERT_EQ(*i, 1);
    ASSERT_EQ(x[100], 1);
    ASSERT_EQ(x.back(), 0);
}

// --------------------------------------------------------------------------------
// Tests 27-28: insert tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test13) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(10);
    iterator i1 = x.begin() + 5;
    x.insert(i1, 23);
    ASSERT_EQ(x.size(), 11);
    ASSERT_EQ(*i1, 23);
    ASSERT_EQ(x[5], 23);
}

// --------------------------------------------------------------------------------
// Tests 29-30: erase tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test14) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_back(i);
    }
    iterator it = x.begin();
    x.erase(it + 4);
    ASSERT_EQ(x[6], 7);
}

// --------------------------------------------------------------------------------
// Tests 31-38: resize tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test15) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_front(i);
    }
    x.resize(20);
    ASSERT_EQ(x.size(), 20);
    ASSERT_EQ(x.front(), 9);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, test16) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_front(i);
    }
    x.resize(5);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.front(), 9);
    ASSERT_EQ(x.back(), 5);
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_front(i);
    }
    x.resize(500);
    ASSERT_EQ(x.size(), 500);
    ASSERT_EQ(x.front(), 99);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, test18) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_front(i);
    }
    x.resize(1000, 5);
    ASSERT_EQ(x.size(), 1000);
    ASSERT_EQ(x.front(), 99);
    ASSERT_EQ(x.back(), 5);
}

// --------------------------------------------------------------------------------
// Tests 39-40: swap tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test19) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_front(i);
    }
    deque_type y;
    for(int i = 0; i < 10; ++i) {
        y.push_back(i);
    }
    x.swap(y);
    ASSERT_EQ(x.front(), 0);
    ASSERT_EQ(x.back(), 9);
    ASSERT_EQ(y.front(), 9);
    ASSERT_EQ(y.back(), 0);
}

// --------------------------------------------------------------------------------
// Tests 41-44: == tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test20) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    deque_type y;
    for(int i = 0; i < 100; ++i) {
        y.push_back(i);
    }
    ASSERT_EQ(x == y, true);
}

TYPED_TEST(DequeFixture, test21) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    deque_type y;
    for(int i = 0; i < 100; ++i) {
        y.push_back(i);
    }
    x.push_back(3);
    y.push_back(5);
    ASSERT_EQ(x == y, false);
    y[100] = 3;
    ASSERT_EQ(x == y, true);
}

// --------------------------------------------------------------------------------
// Tests 45-48: < tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test22) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    deque_type y;
    for(int i = 0; i < 100; ++i) {
        y.push_back(i);
    }
    ASSERT_EQ(x < y, false);
}

TYPED_TEST(DequeFixture, test23) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    deque_type y;
    for(int i = 0; i < 100; ++i) {
        y.push_back(i);
    }
    x.push_back(100);
    y.push_back(101);
    ASSERT_EQ(x < y, true);
}

// --------------------------------------------------------------------------------
// Tests 49-56: other deque constructor tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test24) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.front(), 0);
    ASSERT_EQ(x.back(), 0);
}

TYPED_TEST(DequeFixture, test25) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5, 3);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.front(), 3);
    ASSERT_EQ(x.back(), 3);
}

TYPED_TEST(DequeFixture, test26) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5, 3);
    deque_type y = x;
    ASSERT_EQ(x.size(), y.size());
    ASSERT_EQ(x.front(), y.front());
    ASSERT_EQ(x.back(), y.back());
    ASSERT_NE(x.begin(), y.begin());
}

TYPED_TEST(DequeFixture, test27) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(4, 6);
    deque_type y(x);
    ASSERT_EQ(x.size(), y.size());
    ASSERT_EQ(x.front(), y.front());
    ASSERT_EQ(x.back(), y.back());
    ASSERT_NE(x.begin(), y.begin());
}

// --------------------------------------------------------------------------------
// Tests 57-66: iterator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test28) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(50);
    iterator it = x.begin() + 25;
    ASSERT_EQ(*it, 0);
}

TYPED_TEST(DequeFixture, test29) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x;
    iterator it = x.begin();
    for(int i = 0; i < 10; ++i) {
        x.push_back(i);
    }
    ASSERT_EQ(*it, 0);
    ASSERT_EQ(*(it + 3), x[3]);
}

TYPED_TEST(DequeFixture, test30) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x;
    for(int i = 0; i < 10; ++i) {
        x.push_front(i);
    }
    iterator it = x.begin();
    ASSERT_EQ(*it, 9);
    ASSERT_EQ(*(it + 3), x[3]);
}

TYPED_TEST(DequeFixture, test31) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x;
    ASSERT_EQ(x.size(), 0);
    x.push_back(3);
    x.push_back(4);
    iterator b = x.begin();
    iterator e = x.end();
    ASSERT_EQ(*b, 3);
    ASSERT_EQ(*--e, 4);
}

TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(10);
    iterator it = x.begin();
    for(int i = 0; i < 10; ++i) {
        x[i] = i;
        ASSERT_EQ(x.at(i), i);

    }
    for(int i = 0; i < 10; ++i) {
        ASSERT_EQ(*(it + i), i);

    }
}

// --------------------------------------------------------------------------------
// Tests 67-76: more iterator tests
// --------------------------------------------------------------------------------
TYPED_TEST(DequeFixture, test33) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator       = typename TestFixture::const_iterator;
    const deque_type x(1);
    const_iterator it = x.begin();
    ASSERT_EQ(*it, 0);
}

TYPED_TEST(DequeFixture, test34) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator       = typename TestFixture::const_iterator;
    const deque_type x(5, 5);
    const_iterator it = x.begin();
    ASSERT_EQ(*it, 5);
}

TYPED_TEST(DequeFixture, test35) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator       = typename TestFixture::const_iterator;
    const deque_type x(5, 5);
    const_iterator it = x.begin();
    ASSERT_EQ(x.front(), 5);
    it += 3;
    ASSERT_EQ(*it, 5);
}

TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator       = typename TestFixture::const_iterator;
    const deque_type x(6, 4);
    const_iterator it = x.end();
    it -= 5;
    ASSERT_EQ(*it, 4);
}

// --------------------------------------------------------------------------------
// Tests 77-83: deque of deque tests
// --------------------------------------------------------------------------------

// Replace with my_deque later
my_deque<deque<int>> x;
TEST(DequeFixture, test37) {
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
    ASSERT_EQ(x.begin(), x.end());
}

TEST(DequeFixture, test38) {
    deque<int> in(3, 4);
    x.push_back(in);
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(x.at(0).at(1), 4);
}

TEST(DequeFixture, test39) {
    deque<int> in(3, 1);
    x.push_front(in);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x.at(0).at(1), 1);
}

TEST(DequeFixture, test40) {
    deque<int> in(3, 2);
    x.insert(x.begin() + 1, in);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x.at(1).at(1), 2);
}

TEST(DequeFixture, test41) {
    x.erase(x.begin() + 1);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x.at(1).at(1), 4);
}

TEST(DequeFixture, test42) {
    ASSERT_EQ(x[1][1], 4);
    ASSERT_EQ(x[0][2], 1);
}

TEST(DequeFixture, test43) {
    x.resize(10);
    ASSERT_EQ(x.size(), 10);
}
