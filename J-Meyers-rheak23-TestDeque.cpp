// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>      // deque
#include <numeric>    // iota
#include <stdexcept>  // invalid_argument, out_of_range
#include <stdlib.h>   // srand, rand
#include <time.h>     // time
#include <memory>     // shared_ptr

#include "gtest/gtest.h"

#define NODE_SIZE_BYTES 16
#include "IteratorValidityDeque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test
{
    using deque_type = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types = Types<deque<int>, deque<int, allocator<int>>, my_deque<int, allocator<int>>, my_deque<int>,
      my_deque<int, allocator<int>>, my_deque<int, allocator<int>> >;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types, );
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(3, 2);
    ASSERT_EQ(x.size(), 3u);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_TRUE(equal(b, e, begin({ 2, 2, 2 })));
}

TYPED_TEST(DequeFixture, test4)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const initializer_list<int> x = { 2, 3, 4 };
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({ 2, 3, 4 })));
    const deque_type y = x;
    ASSERT_EQ(y.size(), 3u);
    ASSERT_TRUE(equal(begin(y), end(y), begin({ 2, 3, 4 })));
}

TYPED_TEST(DequeFixture, test5)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(10, 2);
    deque_type y(20, 3);
    ASSERT_EQ(y.size(), 20u);
    y = x;
    ASSERT_EQ(y.size(), 10u);
    ASSERT_EQ(y[1], 2);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_NE(&*begin(x), &*begin(y));
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test6)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(3);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(b, e, begin({ 0, 0, 0 })));
}

TYPED_TEST(DequeFixture, test7)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(3);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(b, e, begin({ 0, 0, 0 })));
}

TYPED_TEST(DequeFixture, test8)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> x = { 2, 3, 4 };
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({ 2, 3, 4 })));
    const deque_type y = x;
    ASSERT_EQ(y.size(), 3u);
    ASSERT_TRUE(equal(begin(y), end(y), begin({ 2, 3, 4 })));
}
// push_back test
TYPED_TEST(DequeFixture, test9)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    ASSERT_EQ(x.size(), 3u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 3u);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test10)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> x = { 2, 3, 4 };
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({ 2, 3, 4 })));
    deque_type y = x;
    ASSERT_EQ(y.size(), 3u);
    ASSERT_TRUE(equal(begin(y), end(y), begin({ 2, 3, 4 })));
}

TYPED_TEST(DequeFixture, test11)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1003, 2);
    deque_type y(2004, 3);
    ASSERT_EQ(y.size(), 2004u);
    y = x;
    ASSERT_EQ(y.size(), 1003u);
    ASSERT_EQ(y[1], 2);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_NE(&*begin(x), &*begin(y));
    ASSERT_EQ(x, y);
}
// push_back and pop_back tests
TYPED_TEST(DequeFixture, test12)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    x.pop_back();
    x.pop_back();
    x.pop_back();
    ASSERT_EQ(x.size(), 0u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 0u);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test13)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(123);
    ASSERT_EQ(x.size(), 123u);
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    ASSERT_EQ(x.size(), 126u);
}

TYPED_TEST(DequeFixture, test14)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_EQ(x.size(), 10u);
    x.resize(5);
    ASSERT_EQ(x.size(), 5u);
    x.resize(8);
    ASSERT_EQ(x.size(), 8u);
    x.resize(15);
    ASSERT_EQ(x.size(), 15u);
    x.resize(50);
    ASSERT_EQ(x.size(), 50u);
}

TYPED_TEST(DequeFixture, test15)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_EQ(x.size(), 10u);
    x.resize(5);
    ASSERT_EQ(x.size(), 5u);
    x.resize(8, 1000);
    ASSERT_EQ(x.size(), 8u);
    x.resize(15, 1000);
    ASSERT_EQ(x.size(), 15u);
    x.resize(50, 1000);
    ASSERT_EQ(x.size(), 50u);
}

TYPED_TEST(DequeFixture, test16)
{
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    const deque_type x(3, 2, allocator_type());
    ASSERT_TRUE(equal(begin(x), end(x), begin({ 2, 2, 2 })));
}

TYPED_TEST(DequeFixture, test17)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(10, 2);
    const deque_type y(10, 2);
    const deque_type z(10, 3);
    ASSERT_EQ(x, y);
    ASSERT_NE(x, z);
    ASSERT_LT(x, z);
    ASSERT_LE(x, y);
    ASSERT_GT(z, x);
    ASSERT_GE(x, y);
}

TYPED_TEST(DequeFixture, test18)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1003, 2);
    deque_type y(2004, 3);
    ASSERT_EQ(y.size(), 2004u);
    y = x;
    ASSERT_EQ(y.size(), 1003u);
    ASSERT_EQ(y.at(1), 2);
    ASSERT_NE(&x.at(0), &y.at(0));
    ASSERT_NE(&*begin(x), &*begin(y));
    ASSERT_EQ(x, y);
}
// front, back, clear, resize, and empty test
TYPED_TEST(DequeFixture, test19)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_EQ(x.size(), 10u);
    x.clear();
    ASSERT_EQ(x.size(), 0u);
    x.resize(8, 1000);
    ASSERT_EQ(x.size(), 8u);
    x[0] = 1;
    x[7] = 7;
    ASSERT_EQ(x.front(), 1);
    ASSERT_EQ(x.back(), 7);
    x.clear();
    ASSERT_TRUE(x.empty());
}

// push_front and pop_front tests
TYPED_TEST(DequeFixture, test20)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_front(3);
    x.push_front(4);
    ASSERT_TRUE(std::equal(begin(x), end(x), begin({ 4, 3, 2 })));
    ASSERT_EQ(x.size(), 3u);
    const deque_type y = x;
    ASSERT_TRUE(std::equal(begin(x), end(x), begin({ 4, 3, 2 })));
    ASSERT_TRUE(std::equal(begin(y), end(y), begin({ 4, 3, 2 })));
    ASSERT_EQ(y.size(), 3u);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test21)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_front(3);
    x.push_front(4);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    ASSERT_EQ(x.size(), 0u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 0u);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test22)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 17; ++i)
    {
        x.push_back(i);
    }
    std::vector<int> ref(17);
    std::iota(std::begin(ref), std::end(ref), 0);
    ASSERT_EQ(x.size(), 17u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 17u);
    ASSERT_EQ(x, y);
    ASSERT_TRUE(std::equal(std::begin(ref), std::end(ref), std::begin(x)));
}

TYPED_TEST(DequeFixture, test23)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 100; ++i)
    {
        x.push_back(i);
    }
    std::vector<int> ref(100);
    std::iota(std::begin(ref), std::end(ref), 0);
    ASSERT_EQ(x.size(), 100u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 100u);
    for (int i = 0; i < 100; ++i)
    {
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test24)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    for (int i = 0; i < 100; ++i)
    {
        if (i % 3 == 0)
            x[i] = 23;
    }
    ASSERT_EQ(x.size(), 100u);
    x.resize(50);
    ASSERT_EQ(x.size(), 50u);
    for (int i = 0; i < 50; ++i)
    {
        if (i % 3 == 0)
        {
            ASSERT_EQ(x[i], 23);
        }
    }
    deque_type y = x;
    ASSERT_EQ(y.size(), 50u);
    for (int i = 0; i < 50; ++i)
    {
        if (i % 3 == 0)
        {
            ASSERT_EQ(y[i], 23);
        }
    }
    x.clear();
    y.clear();
    ASSERT_EQ(x.size(), 0u);
    ASSERT_EQ(y.size(), 0u);
}

TYPED_TEST(DequeFixture, test25)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 17);
    for (int i = 0; i < 100; ++i)
    {
        if (i % 2 == 0)
        {
            x[i] = 23;
        }
    }
    ASSERT_EQ(x.size(), 100u);
    for (int i = 0; i < 25; ++i)
    {
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 75u);

    for (int i = 0; i < 25; ++i)
    {
        x.pop_back();
    }

    ASSERT_EQ(x.size(), 50u);
    ASSERT_EQ(x.front(), 17);
    ASSERT_EQ(x.back(), 23);

    for (int i = 0; i < 50; ++i)
    {
        if (i % 2 == 0)
        {
            ASSERT_EQ(x[i], 17);
        }
        else
        {
            ASSERT_EQ(x[i], 23);
        }
    }

    x.clear();
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test26)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0);
    for (int i = 0; i < 50; ++i)
    {
        if (i % 2 == 0)
        {
            x.push_back(23);
            x.push_front(17);
        }
        else
        {
            x.push_back(17);
            x.push_front(23);
        }
    }
    ASSERT_EQ(x.size(), 100u);
    for (int i = 0; i < 25; ++i)
    {
        x.pop_front();
        x.pop_back();
    }

    ASSERT_EQ(x.size(), 50u);
    ASSERT_EQ(x.front(), 17);
    ASSERT_EQ(x.back(), 23);

    for (int i = 0; i < 50; ++i)
    {
        if (i % 2 == 0)
        {
            ASSERT_EQ(x[i], 17);
        }
        else
        {
            ASSERT_EQ(x[i], 23);
        }
    }

    x.clear();
    ASSERT_EQ(x.size(), 0u);
}
// collatz test using initializer list
TYPED_TEST(DequeFixture, test27)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> x = { 1, 2, 8, 4, 3, 6, 9, 17, 4, 20, 7, 15, 10, 10, 18, 18, 5, 13, 21, 21, 8 };
    const deque_type cache = x;
    unsigned n = 2457;
    unsigned c = 0;
    while (n > 1)
    {
        if (n < 10)
        {
            c += cache[n];
            n = 1;
        }
        else if ((n % 2) == 0)
        {
            n = (n / 2);
            ++c;
        }
        else
        {
            n = (3 * n) + 1;
            ++c;
        }
    }
    ASSERT_EQ(c, 134);
}
// larger test
TYPED_TEST(DequeFixture, test28)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 1023; ++i)
    {
        x.push_back(i);
    }
    std::vector<int> ref(1023);
    std::iota(std::begin(ref), std::end(ref), 0);
    ASSERT_EQ(x.size(), 1023u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 1023u);
    for (int i = 0; i < 1023; ++i)
    {
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test29)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 10023; ++i)
    {
        x.push_back(i);
    }
    std::vector<int> ref(10023);
    std::iota(std::begin(ref), std::end(ref), 0);
    ASSERT_EQ(x.size(), 10023u);
    const deque_type y = x;
    ASSERT_EQ(y.size(), 10023u);
    ASSERT_EQ(x, y);
    ASSERT_TRUE(std::equal(std::begin(ref), std::end(ref), std::begin(x)));
}

TYPED_TEST(DequeFixture, test30)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(23);
    x.push_front(17);
    ASSERT_EQ(x.size(), 2);
    x.pop_front();
    x.pop_back();
    ASSERT_EQ(x.size(), 0);
    const initializer_list<int> y = { 1, 2, 8, 4, 3, 6, 9, 17, 4, 20, 7, 15, 10, 10, 18, 18, 5, 13, 21, 21 };
    x = y;
    ASSERT_EQ(x.size(), 20);
    ASSERT_EQ(x.front(), 1);
    ASSERT_EQ(x.back(), 21);
    ASSERT_EQ(x[9], 20);
    ASSERT_EQ(x.at(10), 7);
    x.clear();
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test31)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    const initializer_list<int> y = { 1, 2, 8, 4, 3, 6, 9, 17, 4, 20, 7, 15, 10, 10, 18, 18, 5, 13, 21, 21 };
    x = y;
    ASSERT_EQ(x.size(), 20);
    x.resize(40);
    ASSERT_EQ(x.size(), 40);
    x.resize(20);
    deque_type z = y;
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(z)));
    x.clear();
    z.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_TRUE(z.empty());
}

TYPED_TEST(DequeFixture, test32)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(65536);
    for (int i = 0; i < 16; ++i)
    {
        x.resize(x.size() / 2);
    }
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, test33)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1);
    for (int i = 0; i < 16; ++i)
    {
        x.resize(x.size() * 2);
    }
    ASSERT_EQ(x.size(), 65536);
}

TYPED_TEST(DequeFixture, test34)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_back(val);
    }
    ASSERT_EQ(x.size(), times);
    for (int i = 0; i < times; ++i)
    {
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test35)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_front(val);
    }
    ASSERT_EQ(x.size(), times);
    for (int i = 0; i < times; ++i)
    {
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test36)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_back(i);
    }
    ASSERT_EQ(x.size(), times);
    for (int i = 0; i < times; ++i)
    {
        ASSERT_EQ(x.at(i), i);
    }
    ASSERT_EQ(x.size(), times);
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test37)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_back(i);
    }
    ASSERT_EQ(x.size(), times);
    for (int i = 0; i < times; ++i)
    {
        ASSERT_EQ(x.at(i), x[i]);
    }
    ASSERT_EQ(x.size(), times);
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test38)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_back(val);
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test39)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 65536;
    for (int i = 0; i < times; ++i)
    {
        x.push_front(val);
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test40)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 10000;
    srand(time(NULL));
    for (int i = 0; i < times; ++i)
    {
        int rand1 = rand() % 100;
        for (int j = 0; j < rand1; ++j)
        {
            x.push_front(val);
        }
        for (int j = 0; j < rand1; ++j)
        {
            x.pop_back();
        }
    }
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test41)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    long val = 4294967296;
    int times = 10000;
    srand(time(NULL));
    for (int i = 0; i < times; ++i)
    {
        int rand1 = rand() % 100;
        for (int j = 0; j < rand1; ++j)
        {
            x.push_back(val);
        }
        for (int j = 0; j < rand1; ++j)
        {
            x.pop_front();
        }
    }
    ASSERT_EQ(x.size(), 0);
}

// tests post ++ and --, < within the same node, swap, insert, erase
TYPED_TEST(DequeFixture, test42)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> y = { 1, 2, 8, 4, 3, 6, 9, 17, 4, 20, 7, 15, 10, 10, 18, 18, 5, 13, 21, 21 };
    const deque_type x = y;
    deque_type z;
    deque_type w;
    deque_type u;

    for (int i = 0; i < x.size(); i++)
    {
        z.push_back(x[i]);
    }

    for (int i = x.size() - 1; i >= 0; i--)
    {
        w.push_front(x[i]);
    }

    for (int i = 0; i < x.size(); i++)
    {
        u.push_front(x[i]);
    }
    u = z;
    ASSERT_TRUE(std::equal(std::begin(u), std::end(u), std::begin(z)));
    auto it_u = u.begin();
    it_u++;
    it_u = u.insert(it_u, 10);
    ASSERT_EQ(*it_u, 10);
    it_u--;
    ASSERT_EQ(*it_u, 1);
    try
    {
        u.at(u.size());
    }
    catch(std::exception e)
    {
    }

    swap(w, z);
    ASSERT_TRUE(z[0] < z[19]);
    ASSERT_TRUE(w[0] < w[1]);
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(z)));
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(w)));
    z[0] = 0;
    z[19] = 100;
    w.swap(z);
    ASSERT_EQ(w[0], 0);
    ASSERT_EQ(w[19], 100);

    // insert test
    auto it = w.begin();
    ++it;
    it = w.insert(it, 10);
    ASSERT_EQ(w[1], 10);

    it = w.begin();
    it = w.insert(it, 23);
    ASSERT_EQ(w[0], 23);
    ASSERT_EQ(w[1], 0);
    ASSERT_EQ(w[2], 10);

    it = w.begin() + 15;
    it = w.insert(it, 2323);
    ASSERT_EQ(w[15], 2323);
    w.erase(w.begin() + 15);
    ASSERT_EQ(w[15], 10);

    it = w.begin() + w.size();
    it = w.insert(it, 50);
    ASSERT_EQ(w[w.size() - 1], 50);

    // erase test
    z.erase(z.begin() + 4);
    ASSERT_EQ(z[4], 6);
    z.erase(z.begin());
    ASSERT_EQ(z[0], 2);
    z.erase(z.begin());
    ASSERT_EQ(z[0], 8);
    z.erase(z.end() - 1);
    ASSERT_EQ(z[z.size() - 1], 21);
    z.erase(z.end() - 1);
    ASSERT_EQ(z[z.size() - 1], 13);
}

// back() const, cbegin(), cend(), front() const
TYPED_TEST(DequeFixture, test43)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> y = { 1, 2, 8, 4, 3, 6, 9, 17, 4, 20, 7, 15, 10, 10, 18, 18, 5, 13, 21, 21 };
    const deque_type x = y;
    const deque_type z = y;
    ASSERT_EQ(z.back(), 21);
    ASSERT_EQ(z.front(), 1);
    ASSERT_TRUE(std::equal(std::cbegin(x), std::cend(x), std::cbegin(z)));
}

TYPED_TEST(DequeFixture, iterator_operators)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(1000);
    std::iota(std::begin(x), std::end(x), 0);

    iterator a = (std::begin(x) + 257);
    iterator b = (std::begin(x) + 733);
    iterator c;

    iterator a1 = a;
    iterator a2 = a;
    iterator a3 = a;
    iterator a4 = a;
    iterator a5 = a;
    iterator a6 = a;
    iterator a7 = a;
    iterator b7 = b;
    iterator a8 = a;
    iterator b8 = b;
    iterator a9 = a;
    iterator b9 = b;
    iterator b10 = b;
    iterator b11 = b;
    iterator a12 = a;
    iterator b12 = b;
    iterator a13 = a;
    iterator b13 = b;

    // From: https://en.cppreference.com/w/cpp/iterator/random_access_iterator
    ASSERT_TRUE((a1 += 476) == b);
    ASSERT_TRUE(std::addressof(a2 += 476) == std::addressof(a2));
    iterator a3a = (a3 + 476);
    iterator a3b = (a3 += 476);
    ASSERT_TRUE(a3a == a3b);
    ASSERT_TRUE((a4 + 476) == (476 + a4));
    ASSERT_TRUE((a5 + (199 + 487)) == (a5 + 199) + 487);
    ASSERT_TRUE((a6 + 0) == a6);
    ASSERT_TRUE((a7 + (476 - 1)) == --b7);
    ASSERT_TRUE((b8 += -476) == a8);
    ASSERT_TRUE((b9 -= 476) == a9);
    ASSERT_TRUE(std::addressof(b10 -= 476) == std::addressof(b10));
    iterator b11a = b11 - 476;
    iterator b11b = b11 -= 476;
    ASSERT_TRUE(b11a == b11b);
    ASSERT_TRUE(a12[476] == *b12);
    ASSERT_TRUE(bool(a13 <= b13));
}

TYPED_TEST(DequeFixture, const_iterator_operators)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x(1000);
    std::iota(std::begin(x), std::end(x), 0);

    const_iterator a = (std::begin(x) + 257);
    const_iterator b = (std::begin(x) + 733);
    const_iterator c;

    const_iterator a1 = a;
    const_iterator a2 = a;
    const_iterator a3 = a;
    const_iterator a4 = a;
    const_iterator a5 = a;
    const_iterator a6 = a;
    const_iterator a7 = a;
    const_iterator b7 = b;
    const_iterator a8 = a;
    const_iterator b8 = b;
    const_iterator a9 = a;
    const_iterator b9 = b;
    const_iterator b10 = b;
    const_iterator b11 = b;
    const_iterator a12 = a;
    const_iterator b12 = b;
    const_iterator a13 = a;
    const_iterator b13 = b;

    // From: https://en.cppreference.com/w/cpp/iterator/random_access_iterator
    ASSERT_TRUE((a1 += 476) == b);
    ASSERT_TRUE(std::addressof(a2 += 476) == std::addressof(a2));
    const_iterator a3a = (a3 + 476);
    const_iterator a3b = (a3 += 476);
    ASSERT_TRUE(a3a == a3b);
    ASSERT_TRUE((a4 + 476) == (476 + a4));
    ASSERT_TRUE((a5 + (199 + 487)) == ((a5 + 199) + 487));
    ASSERT_TRUE((a6 + 0) == a6);
    ASSERT_TRUE((a7 + (476 - 1)) == --b7);
    ASSERT_TRUE((b8 += -476) == a8);
    ASSERT_TRUE((b9 -= 476) == a9);
    ASSERT_TRUE(std::addressof(b10 -= 476) == std::addressof(b10));
    const_iterator b11a = b11 - 476;
    const_iterator b11b = b11 -= 476;
    ASSERT_TRUE(b11a == b11b);
    ASSERT_TRUE(a12[476] == *b12);
    ASSERT_TRUE(bool(a13 <= b13));
}

TYPED_TEST(DequeFixture, iterator_conversions)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x(500);
    std::iota(std::begin(x), std::end(x), 0);
    const deque_type y(std::begin(x), std::end(x));

    ASSERT_TRUE(x.cbegin() == x.begin());
    const_iterator cit = std::begin(x);
    // my_deque<int>::iterator it = std::begin(y);  // compilation fail
    ASSERT_TRUE(x.begin() != y.begin());
    ASSERT_TRUE(cit == x.begin());

    ASSERT_TRUE(x.begin() < (x.cbegin() + 1));
}

TEST(DequeNonTrivialFixture, non_trivial_project)
{
    using deque_type = my_deque<std::shared_ptr<int>>;
    deque_type x;
    std::vector<std::shared_ptr<int>> vec;

    x.push_back(std::make_shared<int>(0));
    x.push_back(std::make_shared<int>(1));
    x.push_back(std::make_shared<int>(2));
    x.push_back(std::make_shared<int>(3));
    x.push_front(std::make_shared<int>(-1));
    x.push_front(std::make_shared<int>(-2));
    x.push_front(std::make_shared<int>(-3));

    vec.push_back(std::make_shared<int>(-3));
    vec.push_back(std::make_shared<int>(-2));
    vec.push_back(std::make_shared<int>(-1));
    vec.push_back(std::make_shared<int>(0));
    vec.push_back(std::make_shared<int>(1));
    vec.push_back(std::make_shared<int>(2));
    vec.push_back(std::make_shared<int>(3));

    auto val_equal = [](const shared_ptr<int>& lhs, const shared_ptr<int>& rhs) {
        return *lhs == *rhs;
    };
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(vec), val_equal));
    x.pop_front();
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(vec)+1, val_equal));
    x.pop_front();
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(vec)+2, val_equal));
    x.pop_front();
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(vec)+3, val_equal));
    x.pop_back();
    ASSERT_TRUE(std::equal(std::begin(x), std::end(x), std::begin(vec)+3, val_equal));

    deque_type y;
    auto thing = std::make_shared<int>(1);
    y.push_back(thing);
    ASSERT_TRUE(std::begin(y)->get() == thing.get());
}

TEST(DequeIteratorFixture, stays_valid)
{
    const int initial_size = 1000;
    my_deque<int> x(initial_size);
    std::iota(std::begin(x), std::end(x), 0);
    my_deque<int>::iterator it = std::begin(x);
    x.resize(4 * initial_size);
    std::iota(std::begin(x) + initial_size, std::end(x), initial_size);

    std::vector<int> comp(4 * initial_size);
    std::iota(std::begin(comp), std::end(comp), 0);
    ASSERT_TRUE(std::equal(it, std::end(x), std::begin(comp)));
}
